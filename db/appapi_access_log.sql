/*
Navicat MySQL Data Transfer

Source Server         : 刘强
Source Server Version : 50553
Source Host           : 192.168.2.106:3306
Source Database       : shuzixingqiu

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-01-08 09:58:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for appapi_access_log
-- ----------------------------
DROP TABLE IF EXISTS `appapi_access_log`;
CREATE TABLE `appapi_access_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `log_type` varchar(255) NOT NULL COMMENT '日志类型',
  `req_access_token` varchar(255) NOT NULL COMMENT '访问授权TOKEN',
  `req_user_auth` varchar(255) NOT NULL COMMENT '用户登录授权TOKEN',
  `req_timestamp` bigint(20) NOT NULL COMMENT '请求参数的时间戳',
  `req_once` varchar(255) NOT NULL COMMENT '请求参数的随机字符串',
  `req_method` varchar(255) NOT NULL COMMENT '请求参数中的请求方法',
  `req_params` varchar(5000) NOT NULL COMMENT '请求的所有参数，json格式',
  `req_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '请求时间',
  `resp_status` int(11) NOT NULL DEFAULT '0' COMMENT '返回结果状态值',
  `resp_content` varchar(5000) DEFAULT NULL COMMENT '返回的结果数据',
  `resp_time` datetime DEFAULT NULL COMMENT '返回时间',
  PRIMARY KEY (`id`),
  KEY `ix_appapi_access_log_type` (`log_type`),
  KEY `ix_appapi_access_log_type_token` (`log_type`,`req_access_token`,`req_timestamp`,`req_once`)
) ENGINE=InnoDB AUTO_INCREMENT=29499 DEFAULT CHARSET=utf8 COMMENT='API访问日志';

-- ----------------------------
-- Table structure for appapi_access_token
-- ----------------------------
DROP TABLE IF EXISTS `appapi_access_token`;
CREATE TABLE `appapi_access_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_access` int(11) NOT NULL COMMENT 'app访问授权',
  `access_token` varchar(255) NOT NULL COMMENT '访问授权TOKEN',
  `secret_token` varchar(255) NOT NULL COMMENT '访问密钥TOKEN',
  `expires_in` int(11) NOT NULL DEFAULT '0' COMMENT '过期时间，0-不过期',
  `client_info` varchar(2000) NOT NULL COMMENT '客户端信息',
  `state` smallint(6) NOT NULL DEFAULT '1' COMMENT '状态，1-正常，-1-失效',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_access_token` (`access_token`),
  KEY `fk_app_access_idx` (`app_access`),
  CONSTRAINT `fk_app_access` FOREIGN KEY (`app_access`) REFERENCES `appapi_app_access` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28715 DEFAULT CHARSET=utf8 COMMENT='API访问授权TOKEN';

-- ----------------------------
-- Table structure for appapi_app_access
-- ----------------------------
DROP TABLE IF EXISTS `appapi_app_access`;
CREATE TABLE `appapi_app_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_token` varchar(255) NOT NULL COMMENT '访问TOKEN',
  `secret_token` varchar(255) NOT NULL COMMENT '密钥TOKEN',
  `remake` varchar(500) NOT NULL COMMENT '备注',
  `state` smallint(6) NOT NULL DEFAULT '1' COMMENT '状态，-1-停用，1-正常',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_access_token` (`access_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='API应用授权密钥';

-- ----------------------------
-- Table structure for appapi_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `appapi_user_auth`;
CREATE TABLE `appapi_user_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL COMMENT '用户ID，用户系统中的标识',
  `access_token` bigint(20) NOT NULL COMMENT '访问授权，关联api_access_token',
  `user_token` varchar(255) NOT NULL COMMENT '登录授权码',
  `state` smallint(6) NOT NULL DEFAULT '1' COMMENT '状态，1-正常，-1-失效',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_user_token` (`user_token`,`user_id`,`access_token`),
  KEY `fk_access_token_idx` (`access_token`),
  CONSTRAINT `fk_access_token` FOREIGN KEY (`access_token`) REFERENCES `appapi_access_token` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=412 DEFAULT CHARSET=utf8 COMMENT='API用户登录授权';
