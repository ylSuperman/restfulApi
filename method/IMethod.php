<?php

namespace appApi\method;

/**
 * 方法的接口约束
 * @package appApi\method
 */
interface IMethod
{
    public function getResult();
    public function getStatus();
    public function getError();
    public function setAccessToken($accessToken);
    public function setParameters($params);
    public function handle();
}