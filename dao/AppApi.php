<?php

namespace appApi\dao;
use app\models\AccessLog;
use app\models\AccessToken;
use app\models\AppAccess;
use app\models\UserAuth;
use appApi\models\AppApiUserToken;

/**
 * app api的数据访问
 * @package appApi\dao
 */
class AppApi
{
    /**
     * 记录访问请求
     * @param string $type 日志类型
     * @param string $accessToken 访问授权TOKEN
     * @param string $userAuth 用户登录授权TOKEN
     * @param int $timestamp 请求参数的时间戳
     * @param string $once 请求参数的随机字符串
     * @param string $method 请求参数中的请求方法
     * @param string $params 请求的所有参数，json格式
     * @return AccessLog
     */
    public static function logRequest($type, $accessToken, $userAuth, $timestamp, $once, $method, $params)
    {
        $log = new AccessLog();
        $log->log_type = $type;
        $log->req_access_token = $accessToken;
        $log->req_user_auth = empty($userAuth) ? '' : $userAuth;
        $log->req_timestamp = $timestamp;
        $log->req_once = $once;
        $log->req_method = empty($method) ? '' : $method;
        $log->req_params = $params;
        $log->save(false);
        return $log;
    }

    /**
     * 记录返回结果
     * @param int $logId
     * @param int $status
     * @param string $content
     * @return int
     */
    public static function logResponse($logId, $status, $content)
    {
        return AccessLog::updateAll(
            ['resp_status' => $status, 'resp_content' => $content],
            "id = :id",
            ['id' => $logId]
        );
    }

    /**
     * 是否重复提交请求
     * @param string $type
     * @param string $accessToken
     * @param int $timestamp
     * @param string $once
     * @return bool
     */
    public static function repeatRequest($type, $accessToken, $timestamp, $once)
    {
        $total = AccessLog::find()
            ->where('log_type = :type AND req_access_token = :token AND req_timestamp = :timestamp AND req_once = :once',
                ['type' => $type, 'token' => $accessToken, 'timestamp' => $timestamp, 'once' => $once])
            ->count();
        if($total > 0) {
            return true;
        }
        return false;
    }

    /**
     * 获取app访问授权信息
     * @param $accessToken
     * @return array|null|AppAccess
     */
    public static function appAccessInfo($accessToken)
    {
        return AppAccess::find()
            ->where('access_token = :token', ['token' => $accessToken])
            ->limit(1)
            ->one();
    }

    /**
     * 获取访问授权信息
     * @param $accessToken
     * @return array|null|AccessToken
     */
    public static function accessTokenInfo($accessToken)
    {
        return AccessToken::find()
            ->where('access_token = :token', ['token' => $accessToken])
            ->limit(1)
            ->one();
    }

    /**
     * 保存访问授权token
     * @param $appAccessId
     * @param $accessToken
     * @param $secretToken
     * @param int $expiresIn
     * @param string $clientInfo
     * @return bool
     */
    public static function addAccessToken($appAccessId, $accessToken, $secretToken, $expiresIn=0, $clientInfo='')
    {
        $access = new AccessToken();
        $access->app_access = $appAccessId;
        $access->access_token = $accessToken;
        $access->secret_token = $secretToken;
        $access->expires_in = $expiresIn;
        $access->client_info = $clientInfo;
        return $access->save(false);
    }

    /**
     * @param $accessToken
     * @param $userToken
     * @return array|null|\yii\db\ActiveRecord|UserAuth
     */
    public static function userAuthInfo($accessToken, $userToken)
    {
        return UserAuth::find()
            ->where('access_token = :accessToken AND user_token = :userToken',
                ['accessToken' => $accessToken, 'userToken' => $userToken])
            ->one();
    }
    /**
     * 活动接口
     * @param $accessToken
     * @param $userToken
     * @return array|null|\yii\db\ActiveRecord|UserAuth
     */
    public static function userTokenInfo($userToken)
    {
        return AppApiUserToken::find()
            ->where('user_token = :userToken',
                ['userToken' => $userToken])
            ->one();
    }

    /**
     * 保存用户授权token
     * @param $accessTokenId
     * @param $userId
     * @param $userToken
     * @return bool
     */
    public static function addUserAuth($accessTokenId, $userId, $userToken)
    {
        $auth = new UserAuth();
        $auth->user_id = $userId;
        $auth->access_token = $accessTokenId;
        $auth->user_token = $userToken;
        return $auth->save(false);
    }
    /**
     * 保存用户授权token
     * @param $accessTokenId
     * @param $userId
     * @param $userToken
     * @return bool
     */
    public static function addUserToken($userId, $userToken)
    {
        $auth = new AppApiUserToken();
        $auth->user_id = $userId;
        $auth->user_token = $userToken;
        return $auth->save(false);
    }
}
/*
'id' => 'ID',
'user_id' => '用户ID，用户系统中的标识',
'access_token' => '访问授权，关联api_access_token',
'user_token' => '登录授权码',
'state' => '状态，1-正常，-1-失效',
'addtime' => '添加时间',
*/