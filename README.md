# 基于YII2 restfulApi 基类

#### 介绍
基于YII2 restfulApi 基类

#### 软件架构
软件架构说明


#### 使用说明

controllers --------- 定义模型类文件
	ApiParameters --- 参数定义类
	RequestFormat --- 数据定义
	ResultResponse -- 响应解析类
	ResultStatus ---- 状态定义
	Utils ----------- 公用函数
	
config -------------- 配置文件
	api_versions----- 版本配置文件
	
controllers --------- 入口流程控制类
	BaseApiController 入口流程基类
	MethodController  统一路径访问类
	TokenController   授权token类
	
dao ----------------- 数据库操作类

method -------------- 接口逻辑处理类
	V010000----------接口方法执行类
	BaseMerhod ------ 接口基类
	IMethod --------- 约束接口


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)