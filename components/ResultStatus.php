<?php

namespace appApi\components;

/**
 * 返回数据状态
 * @package appApi\components
 */
class ResultStatus
{
    /**
     * 成功
     */
    const SUCCESS = 10000;
    /**
     * 不是post请求
     */
    const REQUEST_POST = 100;
    /**
     * URL参数不全
     */
    const URL_PARAM_CANNOT_EMPTY = 101;
    /**
     * access token 错误
     */
    const ACCESS_TOKEN_ERROR = 102;
    /**
     * 重复提交请求
     */
    const REPEAT_REQUEST = 103;
    /**
     * 签名错误
     */
    const SIGN_ERROR = 104;
    /**
     * 用户授权错误
     */
    const USER_TOKEN_ERROR = 105;
    /**
     * 用户授权失效
     */
    const USER_TOKEN_EXPIRES = 106;
    /**
     * 数据格式现只支持：JSON,XML,JSONP
     */
    const REQUEST_FORMAT_ERROR = 107;
    /**
     * 业务数据格式错误
     */
    const POST_BODY_FORMAT_ERROR = 108;
    /**
     * 业务数据不全
     */
    const POST_BODY_PARAM_ERROR = 109;
    /**
     * 城市未开通
     */
    const CITY_NOT_OPEN = 110;
    /**
     * 账户封号
     */
    const USER_TOKEN_DISABLE = 111;
    /**
     * 访问的方法不存在
     */
    const METHOD_NOT_EXIST = 200;

    /**
     * 数据库异常
     */
    const DATABASE_ERROR = 400;

    /**
     * 查询的数据不存在
     */
    const QUERY_DATA_NOT_EXIST = 9998;

    /**
     * 未知错误
     */
    const ERROR = 9999;
}