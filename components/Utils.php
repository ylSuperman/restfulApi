<?php

namespace appApi\components;


class Utils
{
    /**
     * 生成token
     * @return string
     */
    public static function createToken()
    {
        if (function_exists('com_create_guid') === true)
        {
            return md5(com_create_guid());
        }

        return md5(sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X',
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(16384, 20479),
            mt_rand(32768, 49151),
            mt_rand(0, 65535),
            mt_rand(0, 65535),
            mt_rand(0, 65535)));
    }
}