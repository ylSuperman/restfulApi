<?php

namespace appApi\components;


/**
 * 请求数据格式
 * @package appApi\components
 */
class RequestFormat
{
    /**
     * JSON
     */
    const JSON = 'JSON';
    /**
     * XML
     */
    const XML = 'XML';
    /**
     * JSONP
     */
    const JSONP = 'JSONP';
}